﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using SeatID = System.UInt16;
using TripID = System.UInt32;

namespace Busreisen
{
    public class Customer
    {
        public uint ID { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public Dictionary<uint, Seat> SeatsByTripID { get; private set; }

        [JsonIgnore]
        public uint SeatID
        {
            get
            {
                if (MainWindow.Instance.TripManagement != null && MainWindow.Instance.SelectedTrip != null)
                {
                    return SeatsByTripID[MainWindow.Instance.SelectedTrip.ID].ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Customer(uint id, string firstName, string lastName, DateTime birthDate)
        {
            SeatsByTripID = new Dictionary<TripID, Seat>();

            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Birthdate = birthDate;
        }
    }
}
