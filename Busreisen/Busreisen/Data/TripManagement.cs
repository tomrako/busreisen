﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Busreisen
{
    public class TripManagement
    {
        public List<Customer> Customers { get; private set; }
        public List<Trip> Trips { get; private set; }
        public List<TripDestination> Destinations { get; private set; }

        public TripManagement()
        {
            Customers = new List<Customer>();
            Trips = new List<Trip>();
            Destinations = new List<TripDestination>();
        }
    }
}
