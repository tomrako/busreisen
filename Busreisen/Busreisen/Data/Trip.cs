﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Busreisen
{
    public class Trip
    {
        public uint ID { get; private set; }
        public List<Customer> Customers { get; set; }
        public string Destination { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Hotel Hotel { get; set; }

        public Trip(uint id)
        {
            Customers = new List<Customer>();
            ID = id;
        }

        public void CreatePDF(string PDFpath)
        {
            FileStream fs = new FileStream(PDFpath, FileMode.Create);
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            Font bold = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, Font.BOLD);
            Paragraph PageHeader = new Paragraph("Reiseteilnehmer - Übersicht",bold);
            PageHeader.Alignment = Element.ALIGN_CENTER;
            doc.Add(PageHeader);
            doc.Add(new Paragraph("\n\n"));

            doc.Add(new Paragraph("Reise-Nr.: " + ID));
            doc.Add(new Paragraph("Reiseziel: " + Destination));
            doc.Add(new Paragraph("Hotel: " + Hotel));
            doc.Add(new Paragraph("Von: " + StartDate.ToString("dd/MM/yyyy")));
            doc.Add(new Paragraph("Bis: " + EndDate.ToString("dd/MM/yyyy")));
            doc.Add(new Paragraph("\n"));

            PdfPTable table = new PdfPTable(5);

            table.AddCell("Sitz-Nr.");
            table.AddCell("Kunden-Nr.");
            table.AddCell("Name");
            table.AddCell("Vorname");
            table.AddCell("Geburtsdatum");
            
            for (int i = 0; i < Customers.Count; i++)
            {
                Customer customer = Customers[i];
                Seat seat;
                if (customer.SeatsByTripID.TryGetValue(ID, out seat))
                {
                    table.AddCell(seat.ID.ToString());
                }
                else
                {
                    table.AddCell(i.ToString());
                }


                table.AddCell(customer.ID.ToString());
                table.AddCell(customer.LastName);
                table.AddCell(customer.FirstName);
                table.AddCell(customer.Birthdate.ToString("dd/MM/yyyy"));
            }

            doc.Add(table);

            doc.Close();
        }
    }
}
