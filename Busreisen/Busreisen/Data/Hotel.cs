﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Busreisen
{
    public class Hotel
    {
        public string Name { get; private set; }
        [JsonIgnore]
        public ushort Stars { get; set; }

        public Hotel(string name, ushort stars)
        {
            Name = name;
            Stars = stars;
        }

        [JsonProperty("Kategorie")]
        public string StarsToString
        {
            get
            {
                string strStars = string.Empty;
                if (Stars >= 0)
                {
                    for (int i = 0; i < Stars; i++)
                    {
                        strStars += "*";
                    }
                    return strStars;
                }
                else
                {
                    return "";
                }
            }
            set 
            {
                Stars = (ushort)value.Length;
            }
        }

        [JsonIgnore]
        public string NameWithStarsBinding { get { return ToString(); } }

        public override string ToString()
        {
            return Name + " (" + StarsToString + ")";
        }
    }
}
