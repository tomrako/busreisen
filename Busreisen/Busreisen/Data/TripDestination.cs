﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Busreisen
{
    public class TripDestination
    {
        [JsonProperty("Stadt")]
        public string Destination { get; set; }
        public List<Hotel> Hotels { get; private set; }

        public TripDestination()
        {
            Hotels = new List<Hotel>();
        }
    }
}
