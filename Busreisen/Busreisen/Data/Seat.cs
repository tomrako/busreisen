﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Busreisen
{
    public class Seat
    {
        private bool m_taken = false;
        public ushort ID { get; private set; }

        public event EventHandler SeatTakenChanged;

        public bool Taken 
        {
            get { return m_taken; } 
            set 
            {     
                m_taken = value;
                if (SeatTakenChanged != null)
                {
                    SeatTakenChanged.Invoke(this, new EventArgs());
                }
            } 
        }

        public Seat(ushort id, bool taken = false)
        {
            this.ID = id;
            this.Taken = taken;
        }
    }
}
