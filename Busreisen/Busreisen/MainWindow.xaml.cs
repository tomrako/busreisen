﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Busreisen
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static MainWindow _mainWindow;
        public TripManagement TripManagement { get; private set; }
        public Trip SelectedTrip { get { return tripGrid.SelectedItem as Trip; } }

        public static MainWindow Instance { get { return _mainWindow; } }

        public MainWindow()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            _mainWindow = this;

            TripManagement = new TripManagement();
            InitializeComponent();
            seatCtrl.MainWindow = this;

            tripGrid.ItemsSource = TripManagement.Trips;
        }

        System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string dllName = args.Name.Contains(',') ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", "");

            dllName = dllName.Replace(".", "_");

            if (dllName.EndsWith("_resources")) return null;

            System.Resources.ResourceManager rm = new System.Resources.ResourceManager(GetType().Namespace + ".Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());

            byte[] bytes = (byte[])rm.GetObject(dllName);

            return System.Reflection.Assembly.Load(bytes);
        }

        private void MenuItemBookNewCustomer_Click(object sender, RoutedEventArgs e)
        {
            uint newID = IDCache.GetNewCustomerID();
            CustomerWindow window = new CustomerWindow(newID);
            bool? result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                TripManagement.Customers.Add(new Customer(newID, window.textBoxFirstName.Text, window.textBoxLastName.Text, window.datePickerBirthdate.DisplayDate));
            }
            else
            {
                IDCache.CustomerIDs.Remove(newID);
            }
        }

        /// <summary>
        /// Neue Reise erstellen
        /// </summary>
        private void MenuItemAddNewTrip_Click(object sender, RoutedEventArgs e)
        {
            TripWindow window = new TripWindow(TripManagement.Destinations);
            bool? result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                uint newID = IDCache.GetNewTripID();
                Hotel hotel = (Hotel)window.comboboxHotel.SelectedValue;
                Trip trip = new Trip(newID) { Hotel = hotel, Destination = window.comboboxDestination.Text, StartDate = window.datePickerStart.DisplayDate, EndDate = window.datePickerEnd.DisplayDate };
                TripManagement.Trips.Add(trip);
                tripGrid.Items.Refresh();
            }
        }

        private void tripGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Trip trip = (Trip)tripGrid.SelectedItem;
            if (trip == null)
            {
                txtBoxDestination.IsEnabled = txtBoxHotel.IsEnabled = txtBoxCategory.IsEnabled = txtBoxStart.IsEnabled = txtBoxEnd.IsEnabled = seatCtrl.IsEnabled = false;
                txtBoxDestination.Text = "";
                txtBoxHotel.Text = "";
                txtBoxCategory.Text = "";
                txtBoxStart.Text = "";
                txtBoxEnd.Text = "";
                List<Customer> cs = new List<Customer>();
                seatCtrl.Refresh(cs);
                customerGrid.ItemsSource = cs;
                customerGrid.Items.Refresh();
                
            }
            else
            {
                customerGrid.ItemsSource = trip.Customers;
                customerGrid.Items.Refresh();
                seatCtrl.Refresh(trip.Customers);
                txtBoxDestination.IsEnabled = txtBoxHotel.IsEnabled = txtBoxCategory.IsEnabled = txtBoxStart.IsEnabled = txtBoxEnd.IsEnabled = seatCtrl.IsEnabled = true;
                txtBoxDestination.Text = trip.Destination;
                txtBoxHotel.Text = trip.Hotel.Name;
                txtBoxCategory.Text = new string('*', trip.Hotel.Stars);
                txtBoxStart.Text = trip.StartDate.ToString("dd.MM.yy");
                txtBoxEnd.Text = trip.EndDate.ToString("dd.MM.yy");
            }
        }

        /// <summary>
        /// Import
        /// </summary>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = ".json";
            fileDialog.Filter = "JSON (.json)|*.json";
            bool? result = fileDialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                TripDestination dest = ImportManager.ImportTripDestinationsFromFile(fileDialog.FileName);
                if (dest != null)
                {
                    TripDestination existing = TripManagement.Destinations.FirstOrDefault(t => t.Destination == dest.Destination);
                    if (existing != null)
                    {
                        foreach (Hotel hotel in dest.Hotels)
                        {
                            Hotel htl = existing.Hotels.FirstOrDefault(h => h.Name == hotel.Name);
                            if (htl == null)
                            {
                                existing.Hotels.Add(hotel);
                            }
                            else
                            {
                                htl.Stars = hotel.Stars;
                            }
                        }
                    }
                    else
                    {
                        TripManagement.Destinations.Add(dest);
                    }

                    tripGrid.Items.Refresh();
                    if (SelectedTrip != null)
                    {
                        txtBoxCategory.Text = SelectedTrip.Hotel.StarsToString;
                    }
                }
            }
        }

        private void MenuItemChangeCustomer_Click(object sender, RoutedEventArgs e)
        {
            CustomerListWindow window = new CustomerListWindow(this, readOnly: false, caption: "Schließen");
            window.ShowDialog();
        }

        private void MenuItem_DeleteBooking(object sender, RoutedEventArgs e)
        {
            Customer cs = customerGrid.CurrentItem as Customer;

            if (cs != null)
            {
                SelectedTrip.Customers.Remove(cs);
                customerGrid.Items.Refresh();
                seatCtrl.Refresh(SelectedTrip.Customers);
            }
        }

        private void MenuItem_ChangeTrip(object sender, RoutedEventArgs e)
        {
            TripListWindow window = new TripListWindow(TripManagement.Destinations, SelectedTrip);
            bool? result = window.ShowDialog();
            if (result == true)
            {
                TripDestination tripDestination = window.comboboxDestination.SelectedValue as TripDestination;
                SelectedTrip.Destination = tripDestination.Destination;
                SelectedTrip.EndDate = window.datePickerEnd.SelectedDate.Value;
                SelectedTrip.StartDate = window.datePickerStart.SelectedDate.Value;
                SelectedTrip.Hotel = window.comboboxHotel.SelectedValue as Hotel;
                tripGrid.Items.Refresh();
            }
        }

        private void MenuItem_CancelTrip(object sender, RoutedEventArgs e)
        {
            Trip trip = tripGrid.SelectedItem as Trip;

            if (trip != null)
            {
                TripManagement.Trips.Remove(trip);
                tripGrid.Items.Refresh();
            }
        }

        private void PrintTrip_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedTrip != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF|*.pdf";
                saveFileDialog.ShowDialog();
                if (saveFileDialog.FileName != "")
                {
                    SelectedTrip.CreatePDF(saveFileDialog.FileName);
                }
            }
        }

        private void ExportTripManagement(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "rvw|*.rvw";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                string strTripManagement = JsonConvert.SerializeObject(TripManagement);
                using (FileStream fs = new FileStream(saveFileDialog.FileName,FileMode.Create))
                {
                    Byte[] nTripmanagement = new UTF8Encoding(true).GetBytes(strTripManagement); 
                    fs.Write(nTripmanagement,0,nTripmanagement.Length);
                }
            }
        }

        private void ImportTripManagement(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "rvw|*.rvw";
            openFileDialog.ShowDialog();
            if (!String.IsNullOrEmpty(openFileDialog.FileName))
            {
                using (StreamReader sr = File.OpenText(openFileDialog.FileName))
                {
                    IDCache.CustomerIDs.Clear();
                    IDCache.TripIDs.Clear();
                    TripManagement = JsonConvert.DeserializeObject<TripManagement>(sr.ReadToEnd());
                    tripGrid.ItemsSource = TripManagement.Trips;
                    tripGrid.Items.Refresh();

                    printCustomers.IsEnabled = customerMenu.IsEnabled = tripsMenu.IsEnabled = true;
                }
            }
        }

        private void NewTripManagement(object sender, RoutedEventArgs e)
        {
            IDCache.CustomerIDs.Clear();
            IDCache.TripIDs.Clear();
            TripManagement = new TripManagement();
            tripGrid.ItemsSource = TripManagement.Trips;
            tripGrid.Items.Refresh();
            printCustomers.IsEnabled = customerMenu.IsEnabled = tripsMenu.IsEnabled = true;
        }

    }
}