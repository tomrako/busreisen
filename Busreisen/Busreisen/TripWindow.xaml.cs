﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Busreisen
{
    /// <summary>
    /// Interaction logic for TripWindow.xaml
    /// </summary>
    public partial class TripWindow : Window
    {
        public TripWindow(List<TripDestination> destinations)
        {
            InitializeComponent();
            comboboxDestination.ItemsSource = destinations;

            datePickerStart.SelectedDate = DateTime.Now;
            datePickerEnd.SelectedDate = DateTime.Now;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(comboboxDestination.SelectedValue == null)
            {
                MessageBox.Show("Bitte wählen Sie ein Ziel.");
            }
            else if (comboboxHotel.SelectedValue == null)
            {
                MessageBox.Show("Bitte wählen Sie ein Hotel.");
            }
            else
            {
                DialogResult = true;
            }
        }

        private void datePickerStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerEnd.SelectedDate == null || datePickerEnd.SelectedDate < datePickerStart.SelectedDate)
            {
                datePickerEnd.SelectedDate = datePickerStart.SelectedDate;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ComboboxDestination_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TripDestination destination = (TripDestination)comboboxDestination.SelectedValue;
            comboboxHotel.ItemsSource = destination.Hotels;
        }

        private void DatePickerEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerStart.SelectedDate == null || datePickerEnd.SelectedDate < datePickerStart.SelectedDate)
            {
                datePickerStart.SelectedDate = datePickerEnd.SelectedDate;
            }
        }
    }
}
