﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Busreisen
{
    /// <summary>
    /// Interaction logic for CustomerListWindow.xaml
    /// </summary>
    public partial class TripListWindow : Window
    {
        private Trip trip;

        public TripListWindow(List<TripDestination> destinations, Trip trip)
        {
            this.trip = trip;
            InitializeComponent();
            comboboxDestination.ItemsSource = destinations;
            ReiseNr.Content = trip.ID;
            datePickerEnd.SelectedDate = trip.EndDate;
            datePickerStart.SelectedDate = trip.StartDate;

            for (int i = 0; i < destinations.Count; i++)
            {
                if (destinations[i].Destination == trip.Destination)
                {
                    comboboxDestination.SelectedIndex = i;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //if(validation)
            DialogResult = true;
            // else
            //{ show errorr }
        }

        private void datePickerStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerEnd.SelectedDate == null || datePickerEnd.SelectedDate < datePickerStart.SelectedDate)
            {
                datePickerEnd.SelectedDate = datePickerStart.SelectedDate;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        bool blnInitHotel = true;
        private void ComboboxDestination_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (blnInitHotel)
            {
                TripDestination destination = (TripDestination)comboboxDestination.SelectedValue;
                comboboxHotel.ItemsSource = destination.Hotels;

                for (int i = 0; i < destination.Hotels.Count; i++)
                {
                    if (destination.Hotels[i].Name == trip.Hotel.Name)
                    {
                        comboboxHotel.SelectedIndex = i;
                    }
                }

                blnInitHotel = false;
            }
            else
            {
                TripDestination destination = (TripDestination)comboboxDestination.SelectedValue;
                comboboxHotel.ItemsSource = destination.Hotels;
            }
            
        }

        private void DatePickerEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerStart.SelectedDate == null || datePickerEnd.SelectedDate < datePickerStart.SelectedDate)
            {
                datePickerStart.SelectedDate = datePickerEnd.SelectedDate;
            }
        }
    }
}
