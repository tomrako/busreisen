﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Busreisen
{
    /// <summary>
    /// Interaction logic for CustomerListWindow.xaml
    /// </summary>
    public partial class CustomerListWindow : Window
    {
        private MainWindow mainWindow;

        public CustomerListWindow(MainWindow parent, bool readOnly, string caption)
        {
            InitializeComponent();

            mainWindow = parent;
            customerGrid.ItemsSource = mainWindow.TripManagement.Customers;
            okButton.Content = caption;
            lastNameColumn.IsReadOnly = birthdateColumn.IsReadOnly = firstNameColumn.IsReadOnly = readOnly;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Tag = customerGrid.SelectedItem as Customer;
            if (Tag != null || !lastNameColumn.IsReadOnly)
            {
                DialogResult = true;
            }
            else
            {
                MessageBox.Show("Bitte wählen sie einen Kunden aus.");
            }
        }
    }
}
