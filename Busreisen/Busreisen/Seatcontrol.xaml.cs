﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Busreisen
{
    /// <summary>
    /// Interaktionslogik für Seatcontrol.xaml
    /// </summary>
    public partial class SeatControl : UserControl
    {
        public class ButtonSeatPair
        {
            public Button Button { get; private set; }
            public Seat Seat { get; private set; }

            public ButtonSeatPair(Button btn, Seat st)
            {
                Button = btn;
                Seat = st;
            }
        }

        private static SolidColorBrush s_colorGreen = (SolidColorBrush)new BrushConverter().ConvertFrom("#29a329");
        private static SolidColorBrush s_colorRed = (SolidColorBrush)new BrushConverter().ConvertFrom("#b22525");

        public Dictionary<ushort, ButtonSeatPair> Seats { get; private set; }
        public MainWindow MainWindow { get; set; }

        public SeatControl()
        {
            Seats = new Dictionary<ushort, ButtonSeatPair>();

            InitializeComponent();

            for (ushort i = 1; i < 23; i++)
            {
                string strSeatname = "Seat" + (i);
                Button btnSeat = (Button)this.FindName(strSeatname);
                btnSeat.Background = s_colorGreen;
                btnSeat.Tag = i;
                Seat seat = new Seat(i);
                seat.SeatTakenChanged += seat_SeatTakenChanged;
                Seats.Add(i, new ButtonSeatPair(btnSeat, seat));

                // Für Rechts- und Linksklick
                btnSeat.MouseDown += Seat_Click;
                btnSeat.Click += Seat_Click;
            }
        }

        private void seat_SeatTakenChanged(object sender, EventArgs e)
        {
            Seat seat = (Seat)sender;
            Seats[seat.ID].Button.Background = seat.Taken ? s_colorRed : s_colorGreen;
        }

        private void Seat_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.SelectedTrip != null)
            {
                ContextMenu menu = new ContextMenu();

                ushort seatID = (ushort)((Button)sender).Tag;

                if (Seats[seatID].Seat.Taken)
                {
                    MenuItem changeBooking = new MenuItem() { Header = "Umbuchen" };
                    foreach (KeyValuePair<ushort, ButtonSeatPair> btnSeat in Seats.Where(s => !s.Value.Seat.Taken))
                    {
                        MenuItem changeBookingItem = new MenuItem() { Header = "Sitz Nr. " + btnSeat.Key };
                        changeBookingItem.Tag = new KeyValuePair<ushort, ushort>(seatID, btnSeat.Key);
                        changeBooking.Items.Add(changeBookingItem);
                        changeBookingItem.Click += ChangeBooking;
                    }
                    menu.Items.Add(changeBooking);

                    MenuItem deleteBooking = new MenuItem() { Header = "Buchung löschen" };
                    deleteBooking.Click += DeleteBooking;
                    menu.Items.Add(deleteBooking);
                }
                else
                {
                    MenuItem newCustomerBooking = new MenuItem() { Header = "Neukunden buchen" };
                    newCustomerBooking.Click += BookNewCustomer;
                    menu.Items.Add(newCustomerBooking);

                    MenuItem existingCustomerBooking = new MenuItem() { Header = "Bestandskunde buchen" };
                    existingCustomerBooking.Tag = Seats[seatID].Seat;
                    existingCustomerBooking.Click += BookExistingCustomer;
                    menu.Items.Add(existingCustomerBooking);
                }

                menu.Tag = seatID;

                menu.IsOpen = true;
            }
        }

        private void ChangeBooking(object sender, RoutedEventArgs e)
        {
            KeyValuePair<ushort, ushort> oldIdNewId = (KeyValuePair<ushort, ushort>)((MenuItem)sender).Tag;
            ushort oldID = oldIdNewId.Key;
            ushort newID = oldIdNewId.Value;
            Customer customer = MainWindow.SelectedTrip.Customers.FirstOrDefault(c => c.SeatsByTripID[MainWindow.SelectedTrip.ID].ID == oldID);
            Seats[oldID].Seat.Taken = false;
            customer.SeatsByTripID[MainWindow.SelectedTrip.ID] = Seats[newID].Seat;
            Seats[newID].Seat.Taken = true;

            MainWindow.customerGrid.Items.Refresh();
        }

        private void DeleteBooking(object sender, RoutedEventArgs e)
        {
            ushort seatID = (ushort)((ContextMenu)((MenuItem)sender).Parent).Tag;

            Customer cs = MainWindow.SelectedTrip.Customers.FirstOrDefault(c => c.SeatsByTripID[MainWindow.SelectedTrip.ID].ID == seatID);
            cs.SeatsByTripID.Remove(MainWindow.SelectedTrip.ID);
            Seats[seatID].Seat.Taken = false;

            MainWindow.SelectedTrip.Customers.Remove(cs);
            List<Customer> customers = MainWindow.SelectedTrip.Customers.OrderBy(x=>x.SeatID).ToList();
            MainWindow.SelectedTrip.Customers.Clear();
            MainWindow.SelectedTrip.Customers.AddRange(customers);
            MainWindow.customerGrid.Items.Refresh();
        }

        private void BookNewCustomer(object sender, RoutedEventArgs e)
        {
            uint newID = IDCache.GetNewCustomerID();
            CustomerWindow window = new CustomerWindow(newID);
            bool? result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                ushort seatID = (ushort)((ContextMenu)((MenuItem)sender).Parent).Tag;
                Seat seat = Seats[seatID].Seat;
                Customer newCustomer = new Customer(newID, window.textBoxFirstName.Text, window.textBoxLastName.Text, window.datePickerBirthdate.DisplayDate);
                newCustomer.SeatsByTripID.Add(MainWindow.SelectedTrip.ID, seat);
                seat.Taken = true;
                MainWindow.TripManagement.Customers.Add(newCustomer);
                if (MainWindow.SelectedTrip != null)
                {
                    MainWindow.SelectedTrip.Customers.Add(newCustomer);
                    List<Customer> customers = MainWindow.SelectedTrip.Customers.OrderBy(x => x.SeatID).ToList();
                    MainWindow.SelectedTrip.Customers.Clear();
                    MainWindow.SelectedTrip.Customers.AddRange(customers);
                    MainWindow.customerGrid.Items.Refresh();
                }
            }
        }

        private void BookExistingCustomer(object sender, RoutedEventArgs e)
        {
            CustomerListWindow window = new CustomerListWindow(MainWindow, true, "Auswählen");
            window.ShowDialog();

            if (window.Tag != null)
            {
                Customer customer = (Customer)window.Tag;
                Seat oldSeat;
                customer.SeatsByTripID.TryGetValue(MainWindow.SelectedTrip.ID, out oldSeat);
                if (oldSeat != null)
                    oldSeat.Taken = false;
                Seat seat = (Seat)((MenuItem)sender).Tag;
                seat.Taken = true;
                customer.SeatsByTripID[MainWindow.SelectedTrip.ID] = seat;

                if (!MainWindow.SelectedTrip.Customers.Any(i => i.ID == customer.ID))
                { 
                    MainWindow.SelectedTrip.Customers.Add(customer);
                    List<Customer> customers = MainWindow.SelectedTrip.Customers.OrderBy(x => x.SeatID).ToList();
                    MainWindow.SelectedTrip.Customers.Clear();
                    MainWindow.SelectedTrip.Customers.AddRange(customers);
                }
            }

            MainWindow.customerGrid.Items.Refresh();
        }

        internal void Refresh(IList<Customer> customerList)
        {
            foreach (KeyValuePair<ushort, ButtonSeatPair> seatButton in Seats)
            {
                if (MainWindow.SelectedTrip != null)
                {
                    seatButton.Value.Seat.Taken = customerList
                        .Any(c =>
                        {
                            Seat seat;
                            c.SeatsByTripID.TryGetValue(MainWindow.SelectedTrip.ID, out seat);
                            return seat.ID == seatButton.Value.Seat.ID;
                        });
                }
                else
                {
                    seatButton.Value.Seat.Taken = false;
                }
            }
        }
    }
}
