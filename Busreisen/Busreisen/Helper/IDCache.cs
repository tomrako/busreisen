﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Busreisen
{
    public static class IDCache
    {
        public static HashSet<uint> CustomerIDs = new HashSet<uint>();
        public static HashSet<uint> TripIDs = new HashSet<uint>();

        private static uint GetNewID(HashSet<uint> exsisting)
        {
            uint newID = exsisting.Count == 0 ? 1 : exsisting.Max() + 1;
            exsisting.Add(newID);
            return newID;
        }

        public static uint GetNewCustomerID()
        {
            return GetNewID(CustomerIDs);
        }

        public static uint GetNewTripID()
        {
            return GetNewID(TripIDs);
        }
    }
}
