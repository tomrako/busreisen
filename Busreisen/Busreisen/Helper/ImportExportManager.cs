﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace Busreisen
{
    public static class ImportManager
    {
        public static TripDestination ImportTripDestinationsFromFile(string path)
        {
            string json = File.ReadAllText(path);
            return ParseTripDestinations(json);
        }

        public static TripDestination ParseTripDestinations(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<TripDestination>(json);
            }
            catch
            {
                MessageBox.Show("Das json ist fehlerhaft");
                return null;
            }
        }
    }
}
